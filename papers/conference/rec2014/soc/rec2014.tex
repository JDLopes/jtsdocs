\documentclass{IEEEconfA4}

\usepackage[latin1]{inputenc}
\usepackage[dvips]{graphicx}
\usepackage{epsfig}
\usepackage{amsfonts, color}
%\usepackage{subfigure}
\usepackage{eurosym}
\usepackage{url}
\usepackage{tabularx}


%------------------------------------------------------------------------------
\begin{document}

\title{Building Reconfigurable Systems Using Open Source Components}

% For authors with the same affiliation
%\author{Author1, Author2, ...\\
%  \begin{affiliation}
%    Affiliation\\
%    \email{\{author1, author2, ...\} @email.org}
%  \end{affiliation}}

% For authors with different affiliations
\author{Jos\'e T. de Sousa, Carlos A. Rodrigues, Nuno Barreiro, Jo\~ao C. Fernandes\\
  \begin{affiliation}
    INESC-ID Lisboa\\
    \email{\{jose.desousa,carlosaarodrigues,nuno.barreiro,joaocfernandes\}@inesc-id.pt}
  \end{affiliation}}

\date{}

\maketitle

%------------------------------------------------------------------------------

\begin{abstract}
The complexity of reconfigurable systems is in great part the complexity of the hardware-software environment where they are integrated. Many different reconfigurable architectures and respective development tools have been proposed in the literature, but few reconfigurable computing startups have succeeded at creating sustainable business models. The extraordinary infrastructure needed to operate any computer in general, and a reconfigurable computer in particular, takes several years to develop and requires intensive capital investment. This problem is slowly but effectively being addressed in the open source community by making available, free of charge, high quality hardware and software components which innovative companies can use to assemble complex systems. This paper presents both a methodology for creating reconfigurable systems on chip (SoCs), using open source components, and a base SoC created using the proposed methodology. An experimental evaluation of the open source components used and of the system itself is provided. 
\end{abstract}


%------------------------------------------------------------------------------
\section{Introduction}


\subsection{Motivation}

As the world becomes more interconnected, it is time for things, in addition to people, to join the cloud. The {\it Internet of Things}, as it is currently coined, offers a practically infinite number of possibilities for building innovative devices. A piece of semiconductor IP, which can be quickly configured to embody a device drawn from a brilliant engineer's mind, is now, more than ever, an extremely valuable asset. Differentiation normally demands that these Internet things have aggressive specifications in terms of area, performance and power consumption.

Such piece of IP is indeed a SoC. In its 25 years of existence, the IP market evolved from supplying small components to providing complete subsystems with one or more processors and several software components. SoCs normally contain one main IP system, responsible for top-level control, and a bunch of specialized IP subsystems.

Extreme size/performance/power specifications for a programmable IP subsystem are often not achievable using conventional processors. To address this problem, reconfigurable computing techniques have, in the last 20 years, gained relevance \cite{Pocek2013}. However, despite showing incontestable advantages w.r.t.\ good specification trade-offs, reconfigurable machines are still difficult to program and have found little practical use.

\subsection{Problem}

The current situation poses a big problem to new entrants in the semiconductor IP market, especially to the ones promoting reconfigurable systems. The value proposition of a reconfigurable system may be easy to articulate as increased performance per megahertz and square millimeter, but having access to every hardware and software building block needed for a complete system is expensive and/or takes too long.


\subsection{Solution}

The solution may be similar to the one found for Operating Systems (OS's): free open source OS's have emerged which in many aspects are better and more robust than commercial alternatives. The same may be true for open hardware descriptions.

\subsection{Objectives}

This project has two objectives. The first is to investigate whether a SoC built from open source IP cores can be silicon ready and built in a time frame comparable to using commercial IP cores. The second is to use this SoC to host a reconfigurable co-processor. To accomplish these goals, we envision the following steps:
\begin{enumerate}\itemsep2pt
\item build a base SoC using open source components wherever possible;
\item create an automatic build and regression test environment;
\item create a development environment where software components can be easily added, removed or modified;
\item develop and add a reconfigurable co-processor.
\end{enumerate}

\subsection{Outline}

This paper is organized as follows. In section~\ref{sec:socdesign}, a SoC base instance implemented with open source modules is described. section~\ref{sec:verific} describes the verification methods used with the proposed SoC. The programming tools are described in section~\ref{sec:tchain}. In section~\ref{sec:dflow}, the development flow for the proposed SoC is outlined. In section~\ref{sec:reconf}, the reconfigurable computing infrastructure is discussed. Finally, in section~\ref{sec:conc}, the early-stage results of this research and its main conclusions are presented.



%------------------------------------------------------------------------------
\section{SoC design}
\label{sec:socdesign}

In order to illustrate the design of a SoC using open source components, this research used the OpenRISC Platform SoC (ORPSoC) \cite{orpsoc}, which is a template platform for creating SoCs based on the OpenRISC processor \cite{or1k}. ORPSoC is a Python program, currently under development by the OpenRISC community, whose purpose is to help assemble SoCs using pre-existing IP cores. The user describes systems and cores using text configuration files, from which ORPSoC creates a directory tree containing simulation and FPGA emulation environments.

Other open source processors have been carefully considered, namely the Leon 2 processor \cite{StamenkovicWSG06}, and the LatticeMico32 \cite{horst2012latticemico32}, and others. However, decisive success factors are whether there is an active community maintaining the code, and whether the toolchain is comprehensive and open. Leon 2 is no longer maintained as the company that gave origin to it has been acquired, and LatticeMico32 still lacks crucial tools such as a good debug environment. Soft processors that are not completely open such as Microblaze and its few clones cannot even be considered. After weighing few alternatives, OpenRISC was clearly the most advantageous choice.

This paper presents Blackbird, the example SoC shown in Fig.\ \ref{fig:soc}. This SoC is being assembled using a modified version of the ORPSoC program developed by the authors. This design is intended as a base SoC for autonomous and low power embedded systems with DSP capabilities. According to the target application, peripherals may be added or removed, which also means the addition or removal of their respective software drivers.

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=8cm]{drawings/soc.eps}}
    \vspace{0cm}\caption{The Blackbird SoC.}
    \label{fig:soc}
\end{figure}


\subsection{OpenRISC and reconfigurable co-processor}

The OpenRISC processor is a modern RISC processor, featuring 1 DMIPS of performance, similar to a commercial ARM9 core. OpenRISC is equipped with instruction and data caches of configurable size as well as instruction and data MMUs. It supports integer operations and optionally floating-point operations. Interrupts and exceptions of various types are also catered for. The OpenRISC is master of the system's Wishbone \cite{wishbone} bus, where many slave peripherals can be attached.

This research envisions the inclusion of a reconfigurable co-processor in the SoC as shown in Fig.\ \ref{fig:soc}. The co-processor is aimed at boosting system performance, especially in accelerating DSP like functions. The reconfigurable co-processor is a Wishbone slave but requires the crucial capability of accessing the central memory directly without intervention of the processor. For this reason, it possesses a master interface to the SDRAM controller.

\subsection{Boot ROM}

After reset, the processor executes code permanently stored in a Boot ROM. This code typically loads a program stored in a non-volatile memory into the central memory. The Boot ROM is a Wishbone slave.


\subsection{SDRAM controller}

The SRAM controller is the interface to an external central memory which is normally a single or double data rate (DDR) SDRAM. Newer generations of DDRs, such as DDR2 or DDR3, can achieve better performance and higher density in terms of stored bits per unit of area. The SDRAM controller is a Wishbone slave. However, the SDRAM controller is also a slave of the reconfigurable co-processor and must include an arbiter to choose between OpenRISC or co-processor accesses.

\subsection{I2C Master}

The I2C core is used to read and write to an external EEPROM, which serves as a non-volatile memory. I2C is a two-pin serial protocol to access off chip devices. It uses bidirectional clock and data pins. The I2C core is a Wishbone slave and an I2C master of the EEPROM device.

\subsection{UART}

The basic IO mechanism is the RS232 serial protocol implemented by a UART core. Currently, this is the only direct channel that software programs running on OpenRISC can use to communicate with the outside, sending or receiving data. In Blackbird, a setup using the UART at 115kb/s is employed.

\subsection{JTAG}

The interface for loading and debugging programs is the 4-pin JTAG interface. The JTAG pins are connected to the JTAG tap module in the SoC, responsible for implementing the JTAG protocol. The JTAG wires connect to an external pod, which in turn connects to a host computer using, for example, USB or Ethernet. 

The host computer may run the program loader or debugger, or it can simply act as a proxy for another computer where these client programs may be run. A debugger program could also run locally on OpenRISC but, as the memory space for storing programs is a scarce resource, a remote debugger is preferred for embedded systems.

\subsection{Debug module}

The debug module sits between the JTAG tap and the Wishbone bus of which it is a master. It also has a direct interface to OpenRISC in order to monitor and control its execution. Being the second master of the Wishbone bus (OpenRISC is the first), the Debug module can read or write the central memory, which is useful for setting up soft breakpoints, access variables and other debug tasks  \cite{Yawn2012}.

\subsection{Power management}

In order to control the energy consumption of the SoC, a power management unit is needed. Note that this SoC is to be typically used in small battery operated electronic devices, for which autonomy is a crucial feature. The power management unit may, for example, contain timers that periodically wake the system from hibernation to perform tasks; during hibernation there is almost no energy consumption.


%------------------------------------------------------------------------------
\section{SoC verification}
\label{sec:verific}

A processor-based system is a complex system where no exhaustive testing can be guaranteed. At best, the system is exercised in as many ways as possible, trying to replicate real situations and to stress critical design corners. No one-size-fits-all solution exists and a combination of techniques is often employed. The verification environment for the Blackbird SoC is illustrated in Fig.\ \ref{fig:verific} and comprises three SoC verification methods: RTL (Verilog) simulation, SystemC simulation and FPGA emulation.

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=8cm]{drawings/toolinteraction.eps}}
    \vspace{0cm}\caption{Verification environment.}
    \label{fig:verific}
\end{figure}

\subsection{Test program and OpenOCD}
\label{verific:openocd}

Common to the three verification methods is the use of a C++ test bench (Test Program) and a universal on-chip debug tool called OpenOCD \cite{OpenOCD}. Using a C++ test bench provides great flexibility in generating test stimuli for the Device Under Test (DUT) and checking its responses. OpenOCD provides a unified target access mechanism with pre-built configurations supporting many commercial development platforms and boards.

The Test Program communicates with OpenOCD using a networked socket to send OpenOCD commands. A simple Test Program is, for instance, a Telnet client where some target specific commands are manually issued, including commands for loading the program, starting and halting execution, etc.

With OpenOCD, the same Test Program can seamlessly exercise our three targets: the RTL simulation, the SystemC simulation and the FPGA emulator. A GDB client can also connect to OpenOCD for program debugging: the user sets a remote target using a specific OpenOCD port; henceforth, all GDB functionalities can be used to debug remote embedded programs as if they were local (for further details see section~\ref{tchain:debugger}).

\subsection{RTL simulation}

RTL simulation is the most commonly used method for SoCs' verification due to its accuracy, both in terms of timing and electrical behaviors. Timing accuracy is obtained via event driven simulation, where component delays dictate the instants when circuit is re-evaluated. Electrical behavior is modeled by multi-valued logic: not only 0 and 1 states can modeled but also other states such as X (0/1 conflict), U (unknown), Z (high-impedance). The most used language in the industry for RTL design and verification is the Verilog language.

RTL simulation can be slow if applied to large logic circuits such as CPUs. However, it is an indispensable tool to verify the design. Its level of detail is adequate even for the asynchronous parts of the design, namely clock domain crossings. In a Verilog design, a Verilog test bench module is needed for RTL simulation. The test bench generates stimuli to excite the design module and checks the correctness of its responses.

Simulating systems where most interactions are software driven requires a considerable amount of stimulus data. Writing such test benches in Verilog is hard. To solve this problem, a Verilog Procedural Interface (VPI) is normally employed. The VPI is a bridge between the test bench and an external C/C++ program, which actually does the verification, reducing the test bench to the role of a mere proxy.

The VPI is a compiled C/C++ library containing some functions that can be called by the C/C++ test program and some functions that can be called by the Verilog test bench. Using a VPI does not reduce the duration of RTL simulation but greatly simplifies the writing of tests, using new or existing C code.

The Blackbird design is supplied with an RTL simulation environment, employing a VPI bridge.  The Test Program exercises the design via OpenOCD, which communicates with the VPI bridge using a network socket. Given the fact that RTL simulation is slow, the Test Program selects a test suite of an adequately low complexity.


\subsection{SystemC simulation}

As the complexity of subsystems increases, it is not uncommon to run Verilog simulations for several hours or even a few days. In large projects, this long simulation cycles represent a critical schedule bottleneck. Hence, since the need to simulate remains crucial, designers have been forced to seek solutions other than RTL simulation.

The so called Embedded System Level (ESL) approaches have taken off in recent years to allow rapid design and verification of complex SoCs. Among such approaches, the SystemC initiative is one of the most relevant ones. SystemC is a set of C++ libraries, containing classes and methods for designing and verifying digital circuits and systems.

Blackbird is supplied with a SystemC simulation environment. The SystemC model of the design is obtained from the design's Verilog description by running a tool called Verilator. The SystemC model created by Verilator can be used as a class in a C++ program. In our design, a C++ program which instantiates the SoC's SystemC model and communicates with the OpenOCD gateway using a TCP/IP socket is currently under development.

SystemC simulation is cycle accurate and can provide full visibility of all the design signals. The Verilator tool can be configured to dump, while the model runs, the waveforms of those signals in a Value Change Dump (VCD) formatted file. These waveforms can then be visualized using any VCD viewer, including the free GTKWAVE program.

SystemC simulation is much faster than RTL simulation, but its time resolution and signal representation accuracy is considerably lower. The Verilator tool uses a fixed time step to evaluate the circuit and works with only two logic levels, 0 and 1. By contrast, RTL simulation uses a variable time step as a result of applying an event driven algorithm. Nevertheless, most problems can be identified and debugged at the Verilator level, a less onerous scheme.

\subsection{FPGA emulation}
\label{verific:fpga}

SystemC simulation may still be a slow process, as the design must run for a long time before any problems manifest themselves. Ideally one would like to test the actual SoC, not yet available at the design stage. The best alternative is to use a circuit emulator, which is normally implemented with FPGA chips. Emulation provides the fastest verification method and, for circuits intended to run at low frequencies, emulation can even be real-time. As a reference, emulation speeds can be 1 to 2 orders of magnitude slower than real-time for complex SoCs implemented in the latest and densest IC technology nodes.

In spite of its performance, emulation provides poor visibility into the design's internal signals. When a bug is found, the designer first tries to diagnose the problem using software methods and/or logic analyzers to observe the interface signals. If observation of the interface signals is not sufficient, internal signals can be observed in the FPGA, with certain limitations, using an Integrated Logic Analyzer (ILA) such as Xilinx's Chipscope. The main limitations of ILAs are the maximum sampling frequency and the maximum amount of internal SRAM that can be used to store signals for observation.

Blackbird is emulated in the Terasic's DE0 Nano FPGA board, as supported by the ORPSoC program mentioned above. The board communicates with OpenOCD for programming and debugging using an USB cable. OpenOCD views this USB connection as a serial port. The USB cable connects to an USB-JTAG adapter implemented by an FTDI chip on the FPGA board. The FPGA board also communicates with the Test Program for data input/output via an USB/RS232 adapter implemented by  another FTDI chip. The adapter is connected to the host computer by a second USB connection, viewed by the Test Program as a serial port, and is connected to the FPGA board using a 4-wire RS232 connection.

%------------------------------------------------------------------------------
\section{Toolchain}
\label{sec:tchain}

The OpenRISC SoC has a fully operational toolchain for barebone applications\footnote{A barebone application runs directly on the machine, without any operating system, and must {\it know} the underlying hardware.}. Compilation, debugging and profiling are based on the corresponding GNU tools. A proven stable version is available and a state of the art version is currently under development. An architectural simulator was developed by the OpenRISC community and constitutes the fist line of application development for the SoC.

A similar toolchain for Linux is also available, built with the libraries $\mu$Clibc and BusyBox. Like the barebone toolchain, the Linux toolchain has both a stable and a development version.

\subsection{Cross-compiler}
\label{tchain:cross-compiler}

The main barebone cross-compiler used for OpenRISC is GCC. Although an LLVM based version also exists, its status is still experimental and, hence, hardly suited for our purposes. There are essentially two versions endowed with an OpenRISC backend: the stable version built on GCC 4.5.1 and the development version glued to the current GCC development trunk, numbered 4.9.0. The official GCC distribution does not yet support OpenRISC and, therefore, the cross-compiler is available as a patch for the stable version and as a fork for the development version. Since some other divergent versions have emerged in the community, it is worth mentioning that the tested version is the one from the official OpenRISC repository.

Many cross-compilers available for commercial processors are built around GCC but, without violating its GNU General Public License (GPL), include closed static libraries for many key functionalities, namely architectural simulation, IO, target loading and debugger connectivity. One might be tempted to develop clean room interface compatible processors, lured by the availability of GCC and other GNU tools. This is a mistake, as the closed components may be very hard to develop, even tough they represent a small part of the whole.

The OpenRISC cross-compiler uses Newlib and is 100\% open source, allowing for customization and tweaking. This possibility makes a huge difference for the developer, who can reflect hardware changes on the cross-compiler, perhaps not trivially,  but for sure without any kind of reverse engineering. Hardware changes may affect the command line options of the cross-compiler to select particular configurations of the CPU and SoC. There are options to make the compilation aware of the floating-point unit, the clock frequency for a specific board, etc.

\subsection{Debugger}
\label{tchain:debugger}

The GNU debugger (GDB) is also available for the OpenRISC SoC in two versions, 7.2 and 7.6.5. GDB provides an extensive range of facilities for execution tracing, halting on conditions, variable watching, etc. Usually run as a standalone application to debug local programs, GDB can also be run as a client/server application to debug a program in a remote system. The control of the target is left to a server which is accessed by the client via the \verb!target remote!  command. The client/server communication is done over TCP/IP and uses GDB's Remote Server Protocol (RSP).

The RSP server may run on the target system or on any machine that has some means of talking to the target. In our design, the RSP server runs on the same machine as the client and communicates with OpenOCD \cite{OpenOCD} which, in turn, communicates via USB/JTAG with the target and acts as a GDB proxy server. For the client this process is transparent and everything works as if the GDB server were running on the target.

\subsection{Architectural simulator}
\label{tchain:or1ksim}

The Or1ksim \cite{or1ksim} is a highly configurable software simulator for the OpenRISC SoC. Its configuration file supports multiple parameters ranging from IO addresses to hardware modules.
The main advantage of Or1ksim, when compared to hardware simulation methods, is speed. For instance, Linux runs smoothly on the Or1ksim, presenting a fast enough command console to the user. However, an architectural simulator is not cycle-accurate and can only be used to evaluate functional correctness without precise timing information. Or1ksim also includes an RSP server enabling debugging via TCP/IP with a GDB client, as shown in Fig.\ \ref{fig:or1ksim-gdb}.

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=6.8cm]{drawings/or1ksim-gdb.eps}}
    \vspace{0cm}\caption{GDB with Or1ksim.}
    \label{fig:or1ksim-gdb}
\end{figure}



\subsection{Profiler}
\label{tchain:profiler}

The toolchain also packs the GNU profiler, GPROF, to measure and analyze the performance of programs. It records, for each function, the number of calls and the time spent in each call. It also shows information on the hierarchy of function calls. The profiler is an invaluable tool to optimize programs for performance and to debug large and/or complex code.

%------------------------------------------------------------------------------
\section{Development flow}
\label{sec:dflow}

The development flow is outlined in Fig.\ \ref{fig:dflow}. It follows four distinct stages, each with several iterations between development and testing. Given the usual complexity, tests are seldom comprehensive: stepping back to prior stages and finding some previously uncaught issues is quite common.

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=8cm]{drawings/dflow.eps}}
    \vspace{0cm}\caption{Development flow.}
    \label{fig:dflow}
\end{figure}


\subsection{Installation}
\label{dflow:install}

The Blackbird SoC specific deliverables are packaged in several containers: hardware (RTL Verilog, SystemC), GNU toolchain (GCC, GDB), architectural simulator (Or1ksim). But in order to have a fully operational environment, some other tools must also be installed and/or configured, namely an RTL simulator like the free Icarus program, Verilator and OpenOCD. Each package comes with a Makefile and a set of instructions to assist the installation process. Some of the packages also ship a set of tests with some indications as to where the detected problems may be occurring, but the overall procedure of obtaining a sane development system is still somewhat cumbersome.

The installation proved successful on Debian Linux (stable version Wheezy), both for 32 and 64 bits systems. Achieving a fully automated installation and testing of the whole development environment for Debian OS (at least) is one of the goals of this project.

\subsection{Peripherals}
\label{dflow:peripherals}

Once the development environment is successfully built and tested, the hardware customization phase may begin: adding or removing modules from the OpenRISC core, configuring peripherals for the SoC, developing new peripherals suited for the aimed application. This phase includes Verilog writing and/or rewriting, but also entails the upgrading of the test bench, both for its hardware and software components.

The toolchain must be adapted to the new hardware, either via the available options mentioned in section~\ref{tchain:cross-compiler}, or via specific backend configuration files. When new peripherals are developed, the compiler backend may also need to be changed to accommodate the new functionalities.

The added functionalities may bring counterparts to the programming environment, namely by the creation of new software libraries. In that case, the test bench also needs to encompass those changes: programs using the new functions should be written and tested.

\subsection{Application}
\label{dflow:application}

The software application is developed in any favored IDE --- an IDE integration is not provided. There are two straightforward ways of running and debugging the application, each with its own advantages and disadvantages: the Or1ksim architectural simulator and the FPGA SoC emulator. The other simulators, Icarus and Verilator, might be occasionally used to uncover possible hardware design issues, but they have the drawback of a lower speed factor, namely in the case of the RTL simulator.

Application testing with the Or1ksim has the advantage of running on the host computer, having no need for a board. However, the architectural simulator does not run as fast as the FPGA emulator and lacks some of the hardware features, namely the cache simulation. As such, running the application on the FPGA is often necessary for speed and timing accuracy.

In both situations, target debugging is performed using the GDB client, as described in section~\ref{tchain:or1ksim} for Or1ksim and in section~\ref{verific:openocd} for the emulation on FPGA.


\subsection{Optimization}
\label{dflow:optimization}

Finally, once the application is built and debugged, optimization concerns arise. This stage makes extensive use of the profiler, GPROF, to search for redundant code and/or reveal more efficient implementations. The overall goal is to accelerate the application execution, using the FPGA emulation as a benchmark to measure the performance in ``real-time" conditions (as mentioned in section~\ref{verific:fpga}, real-time is only achieved with the actual SoC, hence the quotes).

%------------------------------------------------------------------------------
\section{Reconfigurability}
\label{sec:reconf}

DSP tasks can be performed by fixed hardware accelerators, which is a common practice in embedded systems. However, a reconfigurable co-processor can provide better performance and energy footprint, while keeping the system programmable \cite{sideworks}. In fact, different tasks may use similar hardware and, as such, having multiple hardware accelerators with common subblocks constitutes a waste of silicon area and power. Reconfigurable accelerators can morph in runtime into each of the fixed accelerators, thus saving a huge amount of logic resources and energy spending. Three distinguishing features of the reconfigurable co-processors considered in this paper are: their {\it coarse-grain} nature \cite{mirsky96}; their use of Direct Memory Access (DMA) \cite{Rajamani1996}; their short reconfiguration time \cite{smith2002}.

\subsection{Coarse-grain arrays}
For a long time, reconfigurable computing was associated with {\it fine-grain} arrays, i.e.\ FPGAs, which are almost homogeneous arrays of flexibly interconnected low input count look-up tables (LUTs), used for implementing generic logic functions. By contrast, coarse-grain arrays replace the LUTs with more sizeable structures such as ALUs, multipliers, shifters, etc.

However, FPGA reconfigurability is slow and wasteful in terms of area and power. Moreover, FPGAs often need to be combined with other chips in order to achieve a complete solution.  To avoid multiple chips one could use an FPGA IP core. However, FPGA core startups have failed to succeed as the used blocks are bulky and yield expensive chips. In addition, programming FPGAs requires hardware design expertise which raises the cost of development.

Low node count coarse-grain arrays in the form of IP cores can be a good solution to this problem, but the main difficulty so far has been the fact that compiler technology for coarse-grain arrays is still in its infancy.

If we choose to incorporate a reconfigurable co-processor in Blackbird, we plan to tackle this problem in two ways: investigate methods allowing the compiler to be implemented with existing compiler frameworks such as GCC and LLVM; develop both a language and a compiler for the co-processor. 

The former appears to be a more difficult path, since the compiler depends critically on vectorization techniques which are a difficult research subject. In the latter, the language and compiler must be effective for at least the above mentioned compute kernels, and yet simple enough to make this effort sustainable.

\subsection{DMA use}


Reconfigurable co-processors are extremely useful for processing large amounts of data with a repetitive compute pattern. DSP algorithms such as FFTs, FIR and IIR filters are good examples of the compute patterns that reconfigurable co-processors excel at. These compute kernels operate on well defined data blocks. Most of the acceleration obtained with reconfigurable co-processors comes from the fact that the data-blocks are placed in the multiple internal RAMs of the co-processor, so that they can be accessed in parallel.

With a given configuration, the co-processor implements a computation graph with several parallel data sources, compute nodes and data sinks. If the co-processor is simply attached to the system bus, data must be accessed through the memory hierarchy. Data are accessed in small blocks, moved first to the cache and then to the co-processor. Data blocks produced by the co-processor need to be read by the processor and stored in the central memory using the cache. This process of data movement may take too long and offset the acceleration obtained in the engine itself. A DMA block can solve this problem by accessing the largest possible data blocks with a single memory transaction, efficiently moving data between the co-processor memories and the central memory.

\subsection{Low reconfiguration time}
In order to have a small reconfigurable co-processor, the number of reconfigurable elements should be minimized, which also means that the co-processor needs to reconfigure itself more frequently in order to maximize the use of its elements. In an extreme situation, a fixed co-processor implements all accelerators needed by the algorithm: when one accelerator is active the hardware of the others is idle, which is a common source of inefficiency.

Therefore, Run-Time Reconfiguration (RTR) is needed \cite{burns97}, and the faster the reconfiguration, the better the performance. In the approach we envision, using a coarse-grain array, the configuration words should be kept small enough to fit the implementation of caching schemes similar to instruction caching.


%------------------------------------------------------------------------------
\section{Conclusions and status}
\label{sec:conc}

Assembling a SoC from pre-existing components may seem a trivial job but turns out to be an art on its own, where the whole SoC is much more valuable than the sum of its parts. However, since the different modules are reused in several projects, the components become gradually more stable and reliable, rendering the whole effort orders of magnitude smaller than building hardware and software from scratch. This is the essence of a business model consisting of building original systems from open source components.

One major barrier to entering this market is the open source licensing model of the components. There is a lack of a standard license agreement for open source hardware descriptions \cite{greenbaum2011}, as most open source software licenses are not generally applicable. Most open source IP cores are made available using the GNU Lesser General Public License (LGPL), but adoption by users still lacks expression.

The building of Blackbird is an ongoing process. The status of the project as of November 2013 is reported in the following subsections.

\subsection{Hardware status}

The status of the hardware components is summarized in Table\ \ref{tab:hwfeat}. In an Altera Cyclone 4 device, Blackbird, configured with no floating-point/MAC unit, no reconfigurable co-processor and with 16kB I/D caches and MMUs, uses about 13k LEs. Tests with RTL simulation and FPGA emulation have solely been conducted with the out of the box material, accessible by using the ORPSoC program. The SystemC simulation was practically non-existing and is being developed almost from scratch. The results so far actually confirm that the open source IP cores behave well when ported to a user simulation environment.

\begin{table}[htbp]
  \centering
    \begin{tabular}{|p{2cm}|p{5cm}|}
    \hline 
    {\bf Item} & {\bf Comment} \\
    \hline \hline 
    OpenRISC & OK in SystemC simulation, RTL simulation and FPGA \\
    \hline
    Reconfigurable co-processor & Under development \\
    \hline
    UART & OK in SystemC simulation and FPGA \\
    \hline
    BOOTROM & OK in SystemC simulation \\
    \hline
    I2C master & Open core not OK:  being fixed \\
    \hline
    JTAG & OK in FPGA; SystemC under development, OK in RTL simulation and FPGA \\
    \hline
    SDRAM ctr. & OK in FPGA; Wishbone memory model used in SystemC simulation \\
    \hline
    Power Management & Not integrated \\
    \hline
    \end{tabular}
  \caption{Hardware status}
  \label{tab:hwfeat}
\end{table}

\subsection{Verification status}

The status of the verification components is summarized in Table\ \ref{tab:vrfeat}. It should be noted that no automatic build/test environment has yet been implemented and that these results have been obtained with a few asserted tests. FPGA emulation and RTL simulation have been run as provided by the ORPSoC program. The SystemC simulation environment is being fully developed in the scope of this project. Being a cycle accurate simulator, the SystemC model produced by Verilator can generate code coverage metrics. It can also be coupled with GPROF to obtain preliminary profile data. Due to the Zero Wait State approximation, the profile data is optimistic compared to silicon results.

\begin{table}[htbp]
  \centering
    \begin{tabular}{|p{2cm}|p{5cm}|}
    \hline 
    {\bf Item} & {\bf Comment} \\
    \hline \hline 
    Test program & Not started \\
    \hline
    OpenOCD &  OK with RTL simulation and FPGA \\
    \hline
    RTL simulation & Only out of the box features tested \\
    \hline
    SystemC simulation & SystemC test bench to load and run programs from files developed and in use; integration with OpenOCD not started \\
    \hline
    FPGA emulation &  Only out of the box features tested \\
    \hline
    \end{tabular}
  \caption{Verification status}
  \label{tab:vrfeat}
\end{table}

\subsection{Software status}

The status of the software components is summarized in Table\ \ref{tab:swfeat}. These results have also been obtained with assorted tests, and no systematic build and test environment exists so far. In general, it can be said that these tools appear to be solid enough, which is a tremendous help in attaining this project's goals.

\begin{table}[htbp]
  \centering
    \begin{tabular}{|p{2cm}|p{5cm}|}
    \hline 
    {\bf Item} & {\bf Comment} \\
    \hline \hline 
    GCC for C & Programs compiled correctly including a Linux kernel  \\
    \hline
    GCC for C++ & Simple programs compiled correctly\\
    \hline
    Or1ksim & Runs compiled programs and interacts with GDB correctly \\
    \hline
    GDB & Tested with Or1ksim, RTL and FPGA out of the box setup; tests with SystemC simulation not started\\
    \hline
    Profiler & Unknown status \\
    \hline
    Linux toolchain & Not started \\
    \hline
     Linux OS & Kernels compiled with GCC run correctly on Or1ksim and FPGA \\
    \hline
    \end{tabular}
  \caption{Software status}
  \label{tab:swfeat}
\end{table}

\section*{Acknowledgment}
This work was supported by national funds through FCT, Funda��o para a Ci�ncia e Tecnologia, under project PEst-OE/EEI/LA0021/2013.
%------------------------------------------------------------------------------
\bibliographystyle{unsrt}
\bibliography{BIBFile}
%\nocite{*}

\end{document}
