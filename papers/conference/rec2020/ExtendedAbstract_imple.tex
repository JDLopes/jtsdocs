%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     File: ExtendedAbstract_imple.tex                               %
%     Tex Master: ExtendedAbstract.tex                               %
%                                                                    %
%     Author: Andre Calado Marta                                     %
%     Last modified : 27 Dez 2011                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A Calculation section represents a practical development
% from a theoretical basis.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Implementation}
\label{sec:imple}

The \text{LZ77} \text{IP} is designed to accelerate the deflate algorithm, removing
this workload from the processor. It should be able to produce an index
(distance) to the repeated word sequence very fast. The \text{PNG} profile shows
that most of the execution time is spent in the search of the best match. Even
with software optimizations, this process is still very time consuming.
The \text{LZ77} algorithm can be divided in three parts:

\begin{itemize}
\item the search for the start of the match (first three bytes must match);
\item the matching of the rest of the sequence;
\item the writing of the result;
\end{itemize}

The first two parts repeat until the best sequence is found -- these two 
parts are the most time consuming. The last part is a simple write of 
one or four integers containing the information of the \text{LZ77} 
compression. This part is a single step and can not of course be 
improved. In conclusion the first two parts must be improved, aiming at 
reducing the time spend on the sequence search and matching (length 
calculation), using pipeline and parallelization. 


The solution is to use a CAM-like memory allowing to find the start of the input sequences in parallel, reducing the \text{LZ77} search time. 
With this methodology, the compression time equals the stream 
transmission time. The spatial complexity of the circuit grows 
proportionally to the window size used. 



The datapath consist of three blocks: ``Pointer Search'', ``Length/Distance
Coding'' and ``Output Buffer''. These blocks are serially connected, and have
pipeline implemented to reduce the critical path.  This datapath is very simple,
therefore the hardware system does not need a control unit for the stream
management or for its configuration.  The datastream management is implemented
by a simple combinatorial circuit.  It freezes the system whenever the input is
not valid or the output can not be received by the next system. This control
method is only possible due to the simplicity of the \text{LZ77} algorithm.

The \text{LZ77} compression produces a datastream composed by coded 
literals and coded pointers. These codes in the LodePNG \text{LZ77} are 
saved in integers (32-bit). Keeping the hardware datastream in the same format 
prevents extra work in the software. The software is spared from 
formatting datastream. 

The LZ77 IP inputs a string of bytes and outputs a 128-bit word 
consisting of 4x32-bit \text{LZ77} codes, containing literals and/or 
pointers expressed according to the \text{LZ77} data structure.

\subsection{Shift Register}
\label{section:shiftreg}

The shift-register is the system memory. It implements in hardware the 'sliding
window' concept introduced by the \text{LZ77} algorithm.

\subsection{Pointer Search}
\label{section:myipptrsearch}

The "Pointer Search Block", shown in Figure~\ref{fig:hwgeneralschemePtrS},
searches in the window for the longest matching sequence. It is in the "Pointer
Search Block" block that the repeated sequences are found and the best is
selected and coded as a length/distance pair. Its input connected to the Shift
Register and the output connected to the Length/Distance Coding block. It reads
all the data saved in the shift-register, and compares each byte with the
current byte, allowing all matches to be found in one clock cycle.

\begin{figure}[!h]
  \centering
    \includegraphics[width=.4\textwidth]{Figures/HwLz77GeneralScheme1}
	\caption{Pointer Search Block Diagram}
	\label{fig:hwgeneralschemePtrS}
\end{figure}


The block can be divided in two different sub-blocks: "Word Search" – that
performs the search; "Selector" – that selects the longest sequence;

The "Word Search" is composed of as many "Char Matcher" units as positions in
the Shift Register, which find the sequences.  The block size is proportional to
the window size used. The size of the remaining datapath is independent of the
window size value.



The "Char Matcher" is shown in Figure~\ref{fig:hwgeneralschemeCharMatcher}. Each
block searches a sequence with a distance/offset value pair. Essentially, each
byte in the window will be associated with a "Char Matcher", allowing the
simultaneous search and discovery of any possible sequence in as many cycles as
the length of the sequence.


\begin{figure}[!h]
  \centering
    \includegraphics[width=0.4\textwidth]{Figures/HwLz77LetterMatcher}
	\caption{Char Matcher Block Diagram - 3 pipeline levels}
	\label{fig:hwgeneralschemeCharMatcher}
\end{figure}

A very simplified version of "Char Matcher" logic is presented oin this figure
with the block divided into three pipeline levels. In the first level the
comparisons between different registers from the shift-register are
performed. The second level guarantees that only matches with the minimum size
are valid sequences. The third level marks the state of the "Char Matcher" and
starts/ends the matching process in the correct timing.

The "Char Matcher" only starts the matching process, when no other "Char
Matcher" is running. When a "Char Matcher" is working, it inhibits the others
from starting. This condition is critical to avoid the corruption of the
detected sequences. When all the "Char Matchers" are idle, an idle signal is
generated by the "idle" block. The "Char Matcher" uses this signal to unlock the
matching, switching to working state.

The "Char Matcher" units start to work after they are idle-reset and find an
initial matching of 3 equal bytes, using the current byte and the "preview"
registers, which unlock the matching process. For this initial match of three
bytes, it is required to have already in the shift-register the next two
bytes. These next two bytes are saved in the "Preview" registers. The "Char
Matcher" only compares three bytes in the first match, after this initial match
the comparisons will be of one byte per cycle.

After all the "Char Matcher" units stop working, the "Word Search" block
switches to idle. When the "word search" is idle the longest sequence is
selected as the block output. Since all "Char Matcher" units start working at
same time, the longest sequence will be given by the "Char Matcher" with the
longest execution. When no sequence is found the literal is the selected
output. When a sequence is found the pointer is the selected output.  This block
outputs six signals: three flags (valid, last, pointer) and three different
values (literal, offset, length). The "last" control signal is not shown on the
picture.

The "Word Search" has also implements additional logic that guarantees that the
length for a sequence is restrained to the maximum allowed value. When the
length reaches the maximum value, a flag called "maxlen" restarts the matching,
forcing the block "Word Search" into idle state. This forced change to idle
state ends the current matching, and the next block ("Selector") selects the
sequence with the lowest offset. The "Word Search" starts a search for a new
sequence, immediately after the flag "maxlen" is activated.


\subsection{Length/Distance Coding}
\label{section:myiplz77codes}


The "Length/Distance Coding" (Figure~\ref{fig:hwgeneralschemePtrS}) codifies the
information from the "Pointer Search" block output, that can be one of different
types:
\begin{itemize}
\item The literal;
\item The pointer;
\end{itemize}

% \begin{figure}[!h]
  % \centering
    % \includegraphics[width=.4\textwidth]{Figures/HwLz77Code}
	% \caption{Length/Distance Coding Block Diagram}
	% \label{fig:hwgeneralschemeCode}
% \end{figure}

The code for any literal value, as previously mentioned, is the value
itself. The pointer describes the sequence found in the "Pointer Search"
block. The pointer is a structure (normally denominated "length distance pair"),
constituted by two pairs, the length and the offset values, which are routed
together in the logic. In each pair, the first member is the "base code" of the
value (length or offset), and the second member is the "extra bits", which
distinguish different values sharing the same base code. (more details in \cite{mywork01})
%The operation to
% translate the pair into its correspondent code is presented in the following
% equations:

% \[ basecode=value2code(value);\]
% \[ Extrabit= value - value2minvalue(base\_code)\] 

% Where the function "value2code()" translates the given value (length 
% or offset) into its respective "base code". The function 
% "value2minvalue()" gives the minimum possible value (length or offset) 
% of that "Base Code".

% To distinguish the pointer information from the literal data, the 
% length base code has an increment of 257, thus signalling as a pointer. 
% This turns the "length code" value between 257 and 285. The block is 
% divided in two parts, one for the length and the other for the offset. 
% In order to get the "Base Code", it is required to decrement the value 
% by one. This operation aligns the binary value allowing to separate the 
% lengths/offsets that share the same "Base Code" into groups that can be 
% identified by a bit pattern. A multiplexer driven by the bit pattern 
% selects the correct "Base Code". 



\subsection{Output Buffer}
\label{section:myipwritout}

Since literal and pointer data have different sizes, they require an output
buffer, that groups the output into a constant data size, sending it on an AXI
Stream interface. (more details in \cite{mywork01})
% Figure~\ref{fig:hwgeneralschemeWO} presents the “Output
% Buffer" diagram, designed and described above.

% \begin{figure}[!ht]
  % \centering
    % \includegraphics[width=.4\textwidth]{Figures/HwLz77WriteOut}
	% \caption{Output Buffer Block Scheme}
	% \label{fig:hwgeneralschemeWO}
% \end{figure}


The Output Buffer Block is essentially a Ping Pong Buffer that groups different
size outputs to produce one unique datastream. 
% This block receives one of these
% two types of data (literal or pointer), which will be written in one of the
% buffers, say the Ping buffer. Whenever a buffer is full, the excess data is
% written in the other buffer (Pong buffer). The Ping buffer is flushed (its data
% sent out) in the next clock cycle. Whenever the Ping buffer is flushed his role
% switches to Pong role, and the previous Pong buffer switches to Ping role (Ping
% Pong behaviour). With this behaviour, the arriving data will always be written
% in the Ping buffer and the overflow data will be written in the Pong
% buffer. This process keeps the data order without causing any bottleneck to the
% system.


% Both buffers are composed of a register of four integers. This pair of buffers
% is controlled as one buffer. The flag pointer indicates if the current data is
% one coded literal (one integer) or a coded pointer (four integer). Thus
% selecting the increment used in the counter: one or four; and the number of
% registers that will be enabled. Whenever the data is a literal, only one
% register is selected by the counter to be written.  Otherwise (in the case of
% the pointer) four registers will be selected to be written with the pointer
% data.

% The “flush" block in the diagram, acknowledge the third bit counter value
% change. Whenever it happens (Ping buffer is full) the block output becomes valid
% for the next cycle. These events will select the data from the previous write
% buffer, marking it as valid and flushing it out.